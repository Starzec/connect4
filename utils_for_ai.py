import math
import random
from copy import deepcopy

from utils import *


def drop_token_pvai(board, col, row, token):
    board[row][col] = token


def player_move_pvai(board, pos):
    col = int(math.floor(pos / SQUARE_SIZE)) - PANEL_SPACE
    if is_valid_move(board, col):
        row = find_row_to_drop(board, col)
        drop_token_pvai(board, col, row, PLAYER1_TOKEN)
        return row, col, True
    return -1, -1, False


def get_all_valid_columns(board):
    return [c for c in range(COL_COUNT) if is_valid_move(board, c)]


def give_points(array, token):
    array = list(array)
    score = 0
    for i in range(len(array) - 3):
        cut_array = array[i:i+4]
        if cut_array.count(token) == 4:
            score += 900
        elif cut_array.count(token) == 3 and cut_array.count(FREE_SPACE) == 1:
            score += 5
        elif cut_array.count(token) == 2 and cut_array.count(FREE_SPACE) == 2:
            score += 2
    return score


def evaluate_points(board, row, col, token, score_center):
    score = 0
    row_array = board[row]
    column_array = board[:, col]
    if score_center and col == COL_COUNT//2:
        score += list(column_array).count(token) * 2  # favor center
    major = np.diagonal(board, offset=(col - row))
    minor = np.diagonal(np.rot90(board), offset=-COL_COUNT + (col + row) + 1)
    score += give_points(row_array, token) + give_points(column_array, token) + \
             give_points(major, token) + give_points(minor, token)
    return score


def evaluate_points_whole_board(board, token, score_center):
    score = 0
    for row in range(ROW_COUNT):
        for col in range(COL_COUNT):
            score += evaluate_points(board, row, col, token, score_center)
    return score


def is_terminal_node(board, token, opp_token):
    return is_winning_move_for_whole_board(board, opp_token) or is_winning_move_for_whole_board(board, token) or len(get_all_valid_columns(board)) == 0


def minimax(board, depth, alfa, beta, maximizingPlayer, token, opp_token, a_b_using, score_center):
    valid_locations = get_all_valid_columns(board)
    is_terminal = is_terminal_node(board, token, opp_token)
    if depth == 0 or is_terminal:
        if is_terminal:
            if is_winning_move_for_whole_board(board, token):
                return None, 100000000000000
            elif is_winning_move_for_whole_board(board, opp_token):
                return None, -10000000000000
            else:
                return None, 0
        else:
            return None, evaluate_points_whole_board(board, token, score_center)
    if maximizingPlayer:
        value = -math.inf
        column = random.choice(valid_locations)
        for c in valid_locations:
            r = find_row_to_drop(board, c)
            temp_board = deepcopy(board)
            drop_token_pvai(temp_board, c, r, token)
            score = minimax(temp_board, depth - 1, alfa, beta, False, token, opp_token, a_b_using, score_center)[1]
            if score > value:
                value = score
                column = c
            if a_b_using:
                alfa = max(alfa, value)
                if alfa >= beta:
                    break
        return column, value
    else:
        value = math.inf
        column = random.choice(valid_locations)
        for c in valid_locations:
            r = find_row_to_drop(board, c)
            temp_board = deepcopy(board)
            drop_token_pvai(temp_board, c, r, opp_token)
            score = minimax(temp_board, depth - 1, alfa, beta, True, token, opp_token, a_b_using, score_center)[1]
            if score < value:
                value = score
                column = c
            if a_b_using:
                beta = min(beta, value)
                if alfa >= beta:
                    break
        return column, value


def ai_move_pick_best(board, score_center):
    columns = get_all_valid_columns(board)
    best_score = 0
    best_col = -1
    best_row = -1
    for col in columns:
        row = find_row_to_drop(board, col)
        temp_board = deepcopy(board)
        drop_token_pvai(temp_board, col, row, AI_TOKEN)
        score = evaluate_points(temp_board, row, col, AI_TOKEN, score_center)
        if score > best_score:
            best_score = score
            best_col = col
            best_row = row
    if len(columns) == 0:
        return -1, -1, True
    drop_token_pvai(board, best_col, best_row, AI_TOKEN)
    return best_row, best_col, False


def ai_move_min_max(board, depth, a_b_using, score_center):
    col = minimax(board, depth, -math.inf, math.inf, True, AI_TOKEN, PLAYER1_TOKEN, a_b_using, score_center)[0]
    row = find_row_to_drop(board, col)
    drop_token_pvai(board, col, row, AI_TOKEN)
    return row, col
