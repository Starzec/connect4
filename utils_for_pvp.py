import math

from utils import *


def drop_token_pvp(board, col, row, player):
    number = PLAYER1_TOKEN if player == Player.Player1 else PLAYER2_TOKEN
    board[row][col] = number


def player_move(board, player, pos):
    col = int(math.floor(pos / SQUARE_SIZE)) - PANEL_SPACE
    if is_valid_move(board, col):
        row = find_row_to_drop(board, col)
        drop_token_pvp(board, col, row, player)
        return row, col, True
    return -1, -1, False
