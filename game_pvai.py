from utils_for_ai import *
from utils_for_screen import *


def start(depth, a_b_using=True, score_center=True):
    board = create_board()
    game_running = True
    player = Player.Player1
    move_counter_p1 = 0
    move_counter_p2 = 0

    screen = create_screen(board, 'Player 1', move_counter_p1, move_counter_p2)
    while game_running:
        for event in pygame.event.get():
            draw_top_circle_and_handle_quit(event, screen, player)
            if event.type == pygame.MOUSEBUTTONDOWN:
                if player == Player.Player1:
                    row, col, success = player_move_pvai(board, event.pos[0])
                    if success:
                        if is_winning_move(board, row, col, PLAYER1_TOKEN):
                            game_running = False
                        player = Player.AI
                        move_counter_p1 += 1
                        print(board)
                        screen = create_screen(board, 'AI - 3', move_counter_p1, move_counter_p2)
        if player == Player.AI and game_running:
            row, col = ai_move_min_max(board, depth, a_b_using, score_center)
            if is_winning_move(board, row, col, AI_TOKEN):
                game_running = False
            if game_running:
                player = Player.Player1
            move_counter_p2 += 1
            print(board)
            screen = create_screen(board, 'Player 1', move_counter_p1, move_counter_p2)
    print('GAME FINISHED')
    print('WINNER IS: ' + str(player))
    pygame.time.wait(5000)
    sys.exit()


if __name__ == '__main__':
    start(4, True, True)
