import math

from utils import find_row_to_drop
from utils_for_ai import minimax, drop_token_pvai


def ai_vs_ai_move(board, token, opp_token, depth, a_b_using, score_center):
    col = minimax(board, depth, -math.inf, math.inf, True, token, opp_token, a_b_using, score_center)[0]
    row = find_row_to_drop(board, col)
    drop_token_pvai(board, col, row, token)
    return row, col
