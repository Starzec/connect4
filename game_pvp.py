from utils import *
from utils_for_pvp import player_move
from utils_for_screen import *


def start():
    board = create_board()
    game_running = True
    player = Player.Player1
    move_counter_p1 = 0
    move_counter_p2 = 0

    screen = create_screen(board, 'Player 1', move_counter_p1, move_counter_p2)
    while game_running:
        for event in pygame.event.get():
            draw_top_circle_and_handle_quit(event, screen, player)
            if event.type == pygame.MOUSEBUTTONDOWN:
                if player == Player.Player1:
                    row, col, success = player_move(board, player, event.pos[0])
                    if success:
                        if is_winning_move(board, row, col, PLAYER1_TOKEN):
                            game_running = False
                        player = Player.Player2
                else:
                    row, col, success = player_move(board, player, event.pos[0])
                    if success:
                        if is_winning_move(board, row, col, PLAYER2_TOKEN):
                            game_running = False
                        player = Player.Player1
                print(board)
                if player == Player.Player1:
                    screen = create_screen(board, 'Player 1', move_counter_p1, move_counter_p2)
                else:
                    screen = create_screen(board, 'Player 2', move_counter_p2, move_counter_p1)
    print('GAME FINISHED')
    sys.exit()


if __name__ == '__main__':
    start()
