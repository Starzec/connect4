from enum import Enum
import numpy as np

from constants import *


class Player(Enum):
    Player1 = 1
    Player2 = 2
    AI = 3
    AI_2 = 4


def create_board():
    return np.zeros(BOARD_SHAPE)


def is_valid_move(board, col):
    return not board[0][col]


def find_row_to_drop(board, col):
    for i, row in enumerate(board[:, col][::-1]):  # Take column flip it, and find first occurrence of 0
        if row == FREE_SPACE:
            return ROW_COUNT - 1 - i


def is_winning_move(board, row, col, token):
    counter = 0
    for r in board[row]:
        if r == token:
            counter += 1
            if counter > 3:
                return True
        else:
            counter = 0
    counter = 0
    for c in board[:, col]:
        if c == token:
            counter += 1
            if counter > 3:
                return True
        else:
            counter = 0
    counter = 0
    major = np.diagonal(board, offset=(col - row))
    for e in major:
        if e == token:
            counter += 1
            if counter > 3:
                return True
        else:
            counter = 0
    counter = 0
    minor = np.diagonal(np.rot90(board), offset=-COL_COUNT + (col + row) + 1)
    for e in minor:
        if e == token:
            counter += 1
            if counter > 3:
                return True
        else:
            counter = 0
    return False


def is_winning_move_for_whole_board(board, token):
    for row in range(ROW_COUNT):
        for col in range(COL_COUNT):
            if is_winning_move(board, row, col, token):
                return True
