import sys
import pygame
from constants import *
from utils import Player


def create_screen(board, player: str, counter_one, counter_two):
    pygame.init()
    screen = pygame.display.set_mode(SCREEN_SIZE)
    for c in range(COL_COUNT):
        for r in range(ROW_COUNT):
            pygame.draw.rect(screen, RECT_COLOR,
                             (c * SQUARE_SIZE + PANEL_SPACE * SQUARE_SIZE, r * SQUARE_SIZE + SQUARE_SIZE, SQUARE_SIZE, SQUARE_SIZE))
            if board[r][c] == FREE_SPACE:
                pygame.draw.circle(screen, EMPTY_CIRCLE_COLOR, (
                    int(c * SQUARE_SIZE + SQUARE_SIZE / 2) + PANEL_SPACE * SQUARE_SIZE, int(r * SQUARE_SIZE + SQUARE_SIZE + SQUARE_SIZE / 2)),
                                   RADIUS)
            elif board[r][c] == PLAYER1_TOKEN:
                pygame.draw.circle(screen, PLAYER1_CIRCLE, (
                    int(c * SQUARE_SIZE + SQUARE_SIZE / 2) + PANEL_SPACE * SQUARE_SIZE, int(r * SQUARE_SIZE + SQUARE_SIZE + SQUARE_SIZE / 2)),
                                   RADIUS)
            elif board[r][c] == PLAYER2_TOKEN:
                pygame.draw.circle(screen, PLAYER2_CIRCLE, (
                    int(c * SQUARE_SIZE + SQUARE_SIZE / 2) + PANEL_SPACE * SQUARE_SIZE, int(r * SQUARE_SIZE + SQUARE_SIZE + SQUARE_SIZE / 2)),
                                   RADIUS)
            elif board[r][c] == AI_TOKEN:
                pygame.draw.circle(screen, PLAYER2_CIRCLE, (
                    int(c * SQUARE_SIZE + SQUARE_SIZE / 2) + PANEL_SPACE * SQUARE_SIZE, int(r * SQUARE_SIZE + SQUARE_SIZE + SQUARE_SIZE / 2)),
                                   RADIUS)
            else:
                pygame.draw.circle(screen, PLAYER1_CIRCLE, (
                    int(c * SQUARE_SIZE + SQUARE_SIZE / 2) + PANEL_SPACE * SQUARE_SIZE, int(r * SQUARE_SIZE + SQUARE_SIZE + SQUARE_SIZE / 2)),
                                   RADIUS)
    pygame.draw.rect(screen, PANEL_COLOR, (0, SQUARE_SIZE, SQUARE_SIZE * PANEL_SPACE, (COL_COUNT - 1) * SQUARE_SIZE))
    pygame.draw.rect(screen, (0, 0, 0), (0, 0, SQUARE_SIZE * PANEL_SPACE, SQUARE_SIZE + 10))
    pygame.draw.rect(screen, PANEL_COLOR, (0, 0, SQUARE_SIZE * PANEL_SPACE, SQUARE_SIZE))
    font_counter = pygame.font.Font('freesansbold.ttf', 32)
    text_counter = font_counter.render('First player counter ' + str(counter_one), True, (0, 0, 0), PANEL_COLOR)
    text_counter_rect = text_counter.get_rect(center=(SQUARE_SIZE * PANEL_SPACE/2, (COL_COUNT - 1) * SQUARE_SIZE/2))
    text_counter2 = font_counter.render('Second player counter ' + str(counter_two), True, (0, 0, 0), PANEL_COLOR)
    text_counter_rect2 = text_counter.get_rect(center=(SQUARE_SIZE * (PANEL_SPACE - 0.5) / 2, (COL_COUNT - 1) * SQUARE_SIZE / 2 + SQUARE_SIZE))
    screen.blit(text_counter, text_counter_rect)
    screen.blit(text_counter2, text_counter_rect2)
    font = pygame.font.Font('freesansbold.ttf', 42)
    text = font.render('Move ' + player, True, (0, 0, 0), PANEL_COLOR)
    text_rect = text.get_rect(center=(SQUARE_SIZE * PANEL_SPACE / 2, SQUARE_SIZE / 2))
    screen.blit(text, text_rect)
    pygame.display.update()
    return screen


def draw_top_circle_and_handle_quit(event, screen, player):
    if event.type == pygame.QUIT:
        sys.exit()

    if event.type == pygame.MOUSEMOTION and player != Player.AI:
        pygame.draw.rect(screen, (0, 0, 0), (SQUARE_SIZE * PANEL_SPACE, 0, SCREEN_SIZE[0], SQUARE_SIZE))
        x = event.pos[0]
        pygame.draw.rect(screen, PANEL_COLOR,
                         (0, 0, SQUARE_SIZE * PANEL_SPACE, SQUARE_SIZE))
        font = pygame.font.Font('freesansbold.ttf', 42)
        if player == Player.Player1:
            pygame.draw.circle(screen, PLAYER1_CIRCLE, (x, int(SQUARE_SIZE / 2)), RADIUS)
            player = 'Player 1'
        else:
            pygame.draw.circle(screen, PLAYER2_CIRCLE, (x, int(SQUARE_SIZE / 2)), RADIUS)
        text = font.render('Move ' + player, True, (0, 0, 0), PANEL_COLOR)
        text_rect = text.get_rect(center=(SQUARE_SIZE * PANEL_SPACE / 2, SQUARE_SIZE / 2))
        screen.blit(text, text_rect)
        pygame.display.update()
