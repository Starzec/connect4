import time

from utils_for_ai import *
from utils_for_aivai import ai_vs_ai_move
from utils_for_screen import *
import csv


def start(depth, a_b_using=True, score_center=True):
    start_time = time.time()
    board = create_board()
    game_running = True
    player = random.choice([Player.AI, Player.AI_2])
    move_counter_p1 = 0
    move_counter_p2 = 0
    valid_cols = get_all_valid_columns(board)
    col = random.choice(valid_cols)
    row = find_row_to_drop(board, col)
    if player == Player.AI:
        screen = create_screen(board, 'AI - 3', move_counter_p1, move_counter_p2)
        board[row][col] = AI_TOKEN
        move_counter_p1 += 1
        player = Player.AI_2
    else:
        screen = create_screen(board, 'AI - 4', move_counter_p2, move_counter_p1)
        board[row][col] = AI_2_TOKEN
        move_counter_p2 += 1
        player = Player.AI
    while game_running:
        if player == Player.AI_2 and game_running:
            row, col = ai_vs_ai_move(board, AI_2_TOKEN, AI_TOKEN, depth, a_b_using, score_center)
            if is_winning_move(board, row, col, AI_2_TOKEN):
                game_running = False
            if game_running:
                player = Player.AI
            move_counter_p2 += 1
            print(board)
            screen = create_screen(board, 'AI - 3', move_counter_p1, move_counter_p2)

        if player == Player.AI and game_running:
            row, col = ai_vs_ai_move(board, AI_TOKEN, AI_2_TOKEN, depth, a_b_using, score_center)
            if is_winning_move(board, row, col, AI_TOKEN):
                game_running = False
            if game_running:
                player = Player.AI_2
            move_counter_p1 += 1
            print(board)
            screen = create_screen(board, 'AI - 4', move_counter_p1, move_counter_p2)
    print('GAME FINISHED')
    print('WINNER IS: ' + str(player))
    font = pygame.font.Font('freesansbold.ttf', 46)
    text = font.render('Winner ' + player.name, True, (0, 0, 0), PANEL_COLOR)
    text_rect = text.get_rect(center=(SQUARE_SIZE * PANEL_SPACE / 2, SQUARE_SIZE / 2))
    screen.blit(text, text_rect)
    pygame.display.update()
    if player == Player.Player1:
        return player, move_counter_p1, move_counter_p2, time.time() - start_time
    else:
        return player, move_counter_p2, move_counter_p1, time.time() - start_time


if __name__ == '__main__':
    # min_max with first heuristic
    file = open('data/statistics_minmax_first_heu.csv', 'w', newline='')
    writer = csv.writer(file, delimiter=';')
    writer.writerow(["winner", "winner moves", "loser moves", "time", "depth"])
    for i in range(1, 3):  # For 4 depths
        for j in range(10):  # For 10 games
            winner, winner_moves, loser_moves, time_to_end = start(depth=i, a_b_using=False, score_center=False)
            writer.writerow([winner.name, winner_moves, loser_moves, time_to_end, i])
    file.close()

    # min_max with first heuristic with alfa_beta
    # file = open('data/statistics_alfabeta_first_heu.csv', 'w', newline='')
    # writer = csv.writer(file, delimiter=';')
    # writer.writerow(["winner", "winner moves", "loser moves", "time", "depth"])
    # for i in range(1, 5):  # For 4 depths
    #     for j in range(10):  # For 10 games
    #         winner, winner_moves, loser_moves, time_to_end = start(depth=i, a_b_using=True, score_center=False)
    #         writer.writerow([winner.name, winner_moves, loser_moves, time_to_end, i])
    # file.close()

    # # min_max with second heuristic
    # file = open('data/statistics_minmax_second_heu.csv', 'w', newline='')
    # writer = csv.writer(file, delimiter=';')
    # writer.writerow(["winner", "winner moves", "loser moves", "time", "depth"])
    # for i in range(1, 5):  # For 4 depths
    #     for j in range(10):  # For 10 games
    #         winner, winner_moves, loser_moves, time_to_end = start(depth=i, a_b_using=False, score_center=True)
    #         writer.writerow([winner.name, winner_moves, loser_moves, time_to_end, i])
    # file.close()
#
    # # min_max with second heuristic with alfa_beta
    # file = open('data/statistics_alfabeta_second_heu.csv', 'w', newline='')
    # writer = csv.writer(file, delimiter=';')
    # writer.writerow(["winner", "winner moves", "loser moves", "time", "depth"])
    # for i in range(1, 5):  # For 4 depths
    #     for j in range(10):  # For 10 games
    #         winner, winner_moves, loser_moves, time_to_end = start(depth=i, a_b_using=True, score_center=True)
    #         writer.writerow([winner.name, winner_moves, loser_moves, time_to_end, i])
    # file.close()
